# Binary File Copy Test #

Here is another concurrent file access test. This time I'm writing 100 KB of
binary data in chunks of 1 KB while reading the file from another process as
fast as possible.

As you can see, there are a couple of files with a spread of file sizes (out of
102400 bytes, which they should have if copied fully). That means these were
read partially by the consumer while being written by the producer.

## Example Run ##

```
shell1 $ ./producer.sh

shell2 $: ./consumer.sh
```

## Output ##

```
$ ls -lrt1 | awk '{if ($5<102400) print $5 "\t" $9}'
59392	701.bin
86016	710.bin
62464	954.bin
41984	1062.bin
89088	1514.bin
86016	1663.bin
79872	1711.bin
72704	1928.bin
78848	2059.bin
83968	2079.bin
45056	2087.bin
77824	2152.bin
90112	2483.bin
55296	2521.bin
77824	2754.bin
43008	2901.bin
74752	3273.bin
80896	3463.bin
48128	3584.bin
83968	3693.bin
72704	3874.bin
78848	3922.bin

$ ls -1 | wc -l
2280
```

