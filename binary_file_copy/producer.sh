#!/bin/bash
filename='file.bin'

while [ 1 ]
do
    rm -f ${filename}
    dd if=/dev/zero of=${filename} bs=1k count=100 > /dev/null 2>&1
done

