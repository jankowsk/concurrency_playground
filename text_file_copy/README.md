# Text File Copy Test #

For educative purposes (mainly my own), I have written two small bash scripts
to check what happens when one process reads a file that is being written to by
another process on an XFS filesystem. This exercise is basically one in race
conditions and file locking.

The results is (unsurprisingly) that you end up with three conditions: 1) the
input file does not exist -> no output copy, but more importantly, 2) either
zero (empty copy) or 3) 1 line in the file (i.e. a full copy).

If the file was larger, there would be a spread of file sizes, as demonstrated
in another test.

## Example Run ##

```
shell1 $ ./producer.sh

shell2 $: ./consumer.sh
```

## Output ##

```
$ wc -l $(ls -1rt)
      1 2.txt
      1 3.txt
      1 4.txt
      1 7.txt
      0 8.txt
      1 11.txt
      1 12.txt
      1 13.txt
      1 14.txt
      1 18.txt
      1 19.txt
      1 20.txt
      1 21.txt
      0 22.txt
      1 25.txt
      1 26.txt
      1 27.txt
      1 28.txt
      1 34.txt
<snip>
      1 3862.txt
      1 3866.txt
      1 3867.txt
      1 3868.txt
      1 3871.txt
      1 3872.txt
      1 3876.txt
      1 3877.txt
      1 3878.txt
      1 3882.txt
      1 3883.txt
      1 3884.txt
      1 3885.txt
      1 3889.txt
      1 3890.txt
      1 3891.txt
      1 3892.txt
      1 3895.txt
      1 3896.txt
      1 3897.txt
      1 3898.txt
      1 3904.txt
      1 3905.txt
      1 3906.txt
      1 3907.txt
      1 3910.txt
      1 3914.txt
      1 3915.txt
      0 3916.txt
      1 3921.txt
      1 3922.txt
      1 3923.txt
      1 3927.txt
      1 3928.txt
      0 3929.txt
      1 3933.txt
      1 3934.txt
      1 3938.txt
      1 3939.txt
      1 3940.txt
      1 3941.txt
      1 3946.txt
      1 3947.txt
      1 3948.txt
      0 3949.txt
      1 3952.txt
      1 3953.txt
      0 3954.txt
      1 3956.txt
      1 3957.txt
      1 3958.txt
      1 3963.txt
      1 3964.txt
      1 3965.txt
      1 3969.txt
      1 3970.txt
      1 3971.txt
      1 3972.txt
      1 3975.txt
      1 3976.txt
      1 3977.txt
      0 3978.txt
      1 3981.txt
      1 3982.txt
      1 3983.txt
      1 3984.txt
      1 3988.txt
      1 3989.txt
      1 3990.txt
      1 3991.txt
      1 3994.txt
      1 3995.txt
      1 3996.txt
      1 4000.txt
   1964 total
```

